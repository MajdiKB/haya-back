<?php

namespace App\Http\Controllers;

use App\Models\Houses;
use Illuminate\Http\Request;

class HouseController extends Controller
{
    public function index()
    {
        return Houses::all();
    }

    public function show(Houses $houses)
    {
        return $houses;
    }

    public function store(Request $request)
    {
        $house = Houses::create($request->all());
        return response()->json($house, 201);
    }

    public function update(Request $request, Houses $house)
    {
        $house->update($request->all());

        return response()->json($house, 200);
    }

    public function delete(Houses $house)
    {
        $house->delete();
        return response()->json(null, 204);
    }
}
