<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Houses extends Model
{
    protected $fillable = ['name', 'city', 'type', 'rooms', 'price', 'photo', 'meters', 'bathrooms'];
    use HasFactory;
}
