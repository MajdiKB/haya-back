<?php

use App\Models\Houses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cache;

Route::get('houses', 'HouseController@index');
Route::get('allHouses', 'HouseController@index');
Route::get('house/{house}', 'HouseController@show');
Route::post('house', 'HouseController@store');
Route::put('house/{house}', 'HouseController@update');
Route::delete('house/{house}', 'HouseController@delete');


Route::get('allHouses', function () {
    //le damos tiempo ilimitado al usuario
    //si existe la key en la caché pues se muestran las páginas
    //en este caso, está configurado con forever
    //por lo cual el usuario no tendrá problemas de navegación.
    Cache::forever('cachekey', "Houses");
    if (Cache::has('cachekey')) {
        return Houses::all();
    }
});

Route::get('houses', function () {
    return Houses::paginate(10);
});


//las siguientes líneas no las usamos en el front, pero
//tenemos el CRUD preparado en el back.
Route::get('house/{house}', function ($id) {
    return Houses::find($id);
});

Route::post('house', function (Request $request) {
    $data = $request->all();
    return Houses::create([
        'name' => $data['name'],
        'city' => $data['city'],
        'type' => $data['type'],
        'rooms' => $data['rooms'],
        'price' => $data['price'],
        'photo' => $data['photo'],
        'bathrooms' => $data['bathrooms'],
        'meters' => $data['meters'],
    ]);
});

Route::put('house/{house}', function (Request $request, $id) {
    $house = Houses::findOrFail($id);
    $house->update($request->all());
    return $house;
});

Route::delete('house/{house}', function ($id) {
    Houses::find($id)->delete();
    return response()->json(204);
});
