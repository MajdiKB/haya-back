<?php

namespace Database\Seeders;

use App\Models\Houses;
use Illuminate\Database\Seeder;


class HousesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Houses::create([
            'name' => 'Atico en el centro con vistas al mar',
            'city' => "Barcelona",
            'type' => "Atico",
            'rooms' => 2,
            'price' => 125155,
            'photo' => "https://picsum.photos/300/250",
            'bathrooms' => 1,
            'meters' => 1500,
        ]);
        Houses::create([
            'name' => "Duplex con terraza",
            'city' => "Madrid",
            'type' => "Dúplex",
            'rooms' => 3,
            'price' => 223231,
            'photo' => "https://picsum.photos/300/251",
            'bathrooms' => 3,
            'meters' => 200,
        ]);
        Houses::create([
            'name' => "Chalet en el sur de la isla",
            'city' => "Gran Canaria",
            'type' => "Chalet",
            'rooms' => 6,
            'price' => 995552,
            'photo' => "https://picsum.photos/300/252",
            'bathrooms' => 6,
            'meters' => 800,
        ]);
        Houses::create([
            'name' => "Piso al lado del estadio Mestalla",
            'city' => "Valencia",
            'type' => "Piso",
            'rooms' => 1,
            'price' => 154556,
            'photo' => "https://picsum.photos/300/253",
            'bathrooms' => 2,
            'meters' => 120,
        ]);
        Houses::create([
            'name' => "Chalet adosado en venta en Santiago",
            'city' => "Ossavarry",
            'type' => "Chalet",
            'rooms' => 10,
            'price' => 354556,
            'photo' => "https://picsum.photos/300/254",
            'bathrooms' => 5,
            'meters' => 220,
        ]);
        Houses::create([
            'name' => "Piso en venta en avenida Pintor Felo Mor",
            'city' => "Gran Canaria",
            'type' => "Piso",
            'rooms' => 3,
            'price' => 270000,
            'photo' => "https://picsum.photos/300/255",
            'bathrooms' => 2,
            'meters' => 95,
        ]);
        Houses::create([
            'name' => "Piso en venta en carretera de Chile",
            'city' => "Tenerife",
            'type' => "Piso",
            'rooms' => 1,
            'price' => 170000,
            'photo' => "https://picsum.photos/300/256",
            'bathrooms' => 1,
            'meters' => 70,
        ]);
        Houses::create([
            'name' => "Casa o chalet independiente en venta",
            'city' => "CORNISA",
            'type' => "Casa",
            'rooms' => 5,
            'price' => 1350000,
            'photo' => "https://picsum.photos/300/257",
            'bathrooms' => 5,
            'meters' => 560,
        ]);
        Houses::create([
            'name' => "Atico en venta en Bravo Murillo",
            'city' => "Tenerife",
            'type' => "Piso",
            'rooms' => 3,
            'price' => 590000,
            'photo' => "https://picsum.photos/300/258",
            'bathrooms' => 2,
            'meters' => 204,
        ]);
        Houses::create([
            'name' => "Atico en venta en Cano",
            'city' => "Madrid",
            'type' => "Atico",
            'rooms' => 3,
            'price' => 900000,
            'photo' => "https://picsum.photos/300/259",
            'bathrooms' => 3,
            'meters' => 260,
        ]);
        Houses::create([
            'name' => "Piso en venta en calle Gran Canaria",
            'city' => "Gran Canaria",
            'type' => "Piso",
            'rooms' => 2,
            'price' => 373600,
            'photo' => "https://picsum.photos/300/260",
            'bathrooms' => 2,
            'meters' => 158,
        ]);
        Houses::create([
            'name' => "Piso en venta en calle Sagunto",
            'city' => "La Palma",
            'type' => "Piso",
            'rooms' => 3,
            'price' => 150000,
            'photo' => "https://picsum.photos/300/261",
            'bathrooms' => 2,
            'meters' => 150,
        ]);
    }
}
